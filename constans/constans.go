package constans

import (
	"gitlab.com/fastogt/gofastogt"
)

// Services constants
const DEFAULT_SERVICE_ROOT_DIR_PATH string = "~/streamer"
const MIN_PRICE float64 = 0.0
const DEFAULT_PRICE float64 = MIN_PRICE
const MAX_PRICE float64 = 1000.0

// Streams constans
const DEFAULT_IARC int = 18
const DEFAULT_RESTART_ATTEMPTS int = 10
const DEFAULT_LOOP bool = false
const DEFAULT_HAVE_VIDEO bool = true
const DEFAULT_HAVE_AUDIO bool = true
const DEFAULT_PHOENIX bool = false

const MIN_AUTO_EXIT_TIME int = 1
const MIN_AUDIO_SELECT int = 0
const DEFAULT_RELAY_VIDEO bool = false
const DEFAULT_RELAY_AUDIO bool = false
const MAX_VIDEO_DURATION_MSEC gofastogt.DurationMsec = (60 * 60 * 1000) * 24 * 365

const DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME int = 12 * 3600
const DEFAULT_TIMESHIFT_CHUNK_DURATION int = 120

//stream type const
const PROXY_STR string = "IStream.ProxyStream"
const VOD_PROXY_STR string = "IStream.ProxyStream.ProxyVodStream"
const COD_RELAY_STR string = "IStream.HardwareStream.RelayStream.CodRelayStream"
const COD_ENCODE_STR string = "IStream.HardwareStream.EncodeStream.CodEncodeStream"
const RELAY_STR string = "IStream.HardwareStream.RelayStream"
const ENCODE_STR string = "IStream.HardwareStream.EncodeStream"
const VOD_RELAY_STR string = "IStream.HardwareStream.RelayStream.VodRelayStream"
const VOD_ENCODE_STR string = "IStream.HardwareStream.EncodeStream.VodEncodeStream"
const TIMESHIFT_RECORDER_STR string = "IStream.HardwareStream.RelayStream.TimeshiftRecorderStream"
const TIMESHIFT_PLAYER_STR string = "IStream.HardwareStream.RelayStream.TimeshiftPlayerStream"
const CATCHUP_STR string = "IStream.HardwareStream.RelayStream.TimeshiftRecorderStream.CatchupStream"
const TEST_LIFE_STR string = "IStream.HardwareStream.RelayStream.TestLifeStream"
const CV_DATA_STR string = "IStream.HardwareStream.EncodeStream.CvDataStream"
const CHANGER_RELAY_STR string = "IStream.HardwareStream.EncodeStream.ChangerRelayStream"
const CHANGER_ENCODE_STR string = "IStream.HardwareStream.EncodeStream.ChangerEncodeStream"

func IsVodStream(typeStream string) bool {
	if typeStream == VOD_PROXY_STR ||
		typeStream == VOD_RELAY_STR ||
		typeStream == VOD_ENCODE_STR {
		return true
	}
	return false
}

func IsLifeStream(typeStream string) bool {
	if typeStream == PROXY_STR ||
		typeStream == RELAY_STR ||
		typeStream == ENCODE_STR ||
		typeStream == TIMESHIFT_PLAYER_STR ||
		typeStream == COD_RELAY_STR ||
		typeStream == COD_ENCODE_STR {
		return true
	}
	return false
}

func IsCathupStream(typeStream string) bool {
	return typeStream == CATCHUP_STR
}
