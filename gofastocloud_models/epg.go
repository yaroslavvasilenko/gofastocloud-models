package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud/epg"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type EpgUrl struct {
	ID  primitive.ObjectID `bson:"_id" json:"id"`
	Url string             `bson:"url" json:"url"`
}

func MakeEpgUrl(url *string) *EpgUrl {
	return &EpgUrl{
		ID:  primitive.NewObjectID(),
		Url: *url,
	}
}

func (epgUrl *EpgUrl) ToFront() *front.EpgUrlFront {
	return &front.EpgUrlFront{
		ID:  epgUrl.ID.Hex(),
		Url: epgUrl.Url,
	}
}

type EpgFileds struct {
	Name             string                     `bson:"name"               json:"name"`
	Host             gofastogt.HostAndPort      `bson:"host"               json:"host"`
	URLS             []EpgUrl                   `bson:"urls"               json:"urls"`
	Providers        []ProviderPair             `bson:"providers"          json:"providers"`
	Monitoring       bool                       `bson:"monitoring"         json:"monitoring"`
	AutoStart        bool                       `bson:"auto_start"         json:"auto_start"`
	ActivationKey    string                     `bson:"activation_key"     json:"activation_key"`
	AutoUpdate       bool                       `bson:"auto_update"        json:"auto_update"`
	AutoUpdatePeriod gofastogt.UtcTimeMsec      `bson:"auto_update_period" json:"auto_update_period"`
	Stats            []epg.ServiceStatisticInfo `bson:"stats"              json:"stats"`

	// server setup
	CreatedDate *time.Time `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

func NewEpgFileds() *EpgFileds {
	current := time.Now()
	return &EpgFileds{
		Name: "Epg",
		Host: gofastogt.HostAndPort{Host: "127.0.0.1",
			Port: 4317},
		URLS:        []EpgUrl{},
		Providers:   []ProviderPair{},
		CreatedDate: &current,
		AutoUpdate:  false,
		Stats:       []epg.ServiceStatisticInfo{},
	}
}

func DefaultEPGServiceStatisticInfo() *epg.ServiceStatisticInfo {
	return &epg.ServiceStatisticInfo{
		Cpu:           0.0,
		Gpu:           0.0,
		LoadAverage:   "",
		MemoryTotal:   0,
		MemoryFree:    0,
		HddTotal:      0,
		HddFree:       0,
		BandwidthIn:   0,
		BandwidthOut:  0,
		Uptime:        0,
		Timestamp:     0,
		TotalBytesIn:  0,
		TotalBytesOut: 0,
		OnlineUsers:   epg.OnlineUsers{Daemon: 0},
	}
}

type Epg struct {
	ID        primitive.ObjectID `bson:"_id" json:"id"`
	EpgFileds `bson:",inline"`
}

func (epg *Epg) ToFront(prov []front.ProviderPairFront) *front.EpgFront {
	hexed := epg.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if epg.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*epg.CreatedDate)
	}
	urls := []front.EpgUrlFront{}
	for _, epgUrl := range epg.URLS {
		urls = append(urls, *epgUrl.ToFront())
	}

	return &front.EpgFront{
		ID:                   &hexed,
		Name:                 epg.Name,
		Host:                 epg.Host,
		URLS:                 urls,
		Providers:            prov,
		CreatedDate:          &created,
		Monitoring:           epg.Monitoring,
		AutoStart:            epg.AutoStart,
		ActivationKey:        epg.ActivationKey,
		AutoUpdate:           epg.AutoUpdate,
		AutoUpdatePeriod:     epg.AutoUpdatePeriod,
		ServiceStatisticInfo: *DefaultEPGServiceStatisticInfo(),
	}
}
