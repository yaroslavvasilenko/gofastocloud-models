package gofastocloud_models

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/fastogt/gofastocloud/media"
	con "gitlab.com/fastogt/gofastocloud_models/constans"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ServiceFields struct {
	Streams             []primitive.ObjectID         `bson:"streams"              json:"streams"`
	Series              []primitive.ObjectID         `bson:"series"               json:"series"`
	Providers           []ProviderPair               `bson:"providers"            json:"providers"`
	Name                string                       `bson:"name"                 json:"name"`
	Host                gofastogt.HostAndPort        `bson:"host"                 json:"host"`
	HttpHost            gofastogt.HostAndPort        `bson:"http_host"            json:"http_host"`
	VODsHost            gofastogt.HostAndPort        `bson:"vods_host"            json:"vods_host"`
	CODsHost            gofastogt.HostAndPort        `bson:"cods_host"            json:"cods_host"`
	NginxHost           gofastogt.HostAndPort        `bson:"nginx_host"           json:"nginx_host"`
	RtmpHost            gofastogt.HostAndPort        `bson:"rtmp_host"            json:"rtmp_host"`
	FeedbackDirecotry   string                       `bson:"feedback_directory"   json:"feedback_directory"`
	TimeshiftsDirectory string                       `bson:"timeshifts_directory" json:"timeshifts_directory"`
	HlsDirectory        string                       `bson:"hls_directory"        json:"hls_directory"`
	VodsDirectory       string                       `bson:"vods_directory"       json:"vods_directory"`
	CodsDirectory       string                       `bson:"cods_directory"       json:"cods_directory"`
	ProxyDirectory      string                       `bson:"proxy_directory"      json:"proxy_directory"`
	DataDirectory       string                       `bson:"data_directory"       json:"data_directory"`
	PricePackage        float64                      `bson:"price_package"        json:"price_package"`
	AutoStart           bool                         `bson:"auto_start"           json:"auto_start"`
	Visible             bool                         `bson:"visible"              json:"visible"`
	ActivationKey       string                       `bson:"activation_key"       json:"activation_key"`
	Monitoring          bool                         `bson:"monitoring"           json:"monitoring"`
	Description         string                       `bson:"description"          json:"description"`
	Stats               []media.ServiceStatisticInfo `bson:"stats"                json:"stats"`
	PID                 string                       `bson:"pid"                  json:"pid"`

	// server setup
	CreatedDate *time.Time `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

type Service struct {
	ID            primitive.ObjectID `bson:"_id"     json:"id"`
	ServiceFields `bson:",inline"`
}

func NewServiceFields() *ServiceFields {
	current := time.Now()
	return &ServiceFields{
		Name: "Service",
		Host: gofastogt.HostAndPort{Host: "127.0.0.1",
			Port: 6317},
		HttpHost: gofastogt.HostAndPort{Host: "0.0.0.0",
			Port: 8000},
		VODsHost: gofastogt.HostAndPort{Host: "0.0.0.0",
			Port: 7000},
		CODsHost: gofastogt.HostAndPort{Host: "0.0.0.0",
			Port: 6001},
		NginxHost: gofastogt.HostAndPort{Host: "0.0.0.0",
			Port: 81},
		RtmpHost: gofastogt.HostAndPort{Host: "0.0.0.0",
			Port: 1935},
		FeedbackDirecotry:   con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/feedback",
		TimeshiftsDirectory: con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/timeshifts",
		HlsDirectory:        con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/hls",
		VodsDirectory:       con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/vods",
		CodsDirectory:       con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/cods",
		ProxyDirectory:      con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/proxy",
		DataDirectory:       con.DEFAULT_SERVICE_ROOT_DIR_PATH + "/data",
		PricePackage:        con.DEFAULT_PRICE,
		CreatedDate:         &current,
		AutoStart:           false,
		Visible:             true,
		Monitoring:          false,
		Stats:               []media.ServiceStatisticInfo{},
		PID:                 "ABCD",
	}
}

func DefaultRuntimeServerStats() *media.ServiceStatisticInfo {
	return &media.ServiceStatisticInfo{
		Cpu:           0.0,
		Gpu:           0.0,
		LoadAverage:   "",
		MemoryTotal:   0,
		MemoryFree:    0,
		HddTotal:      0,
		HddFree:       0,
		BandwidthIn:   0,
		BandwidthOut:  0,
		Uptime:        0,
		Timestamp:     0,
		TotalBytesIn:  0,
		TotalBytesOut: 0,
	}
}

func (s *Service) ToFront(prov []front.ProviderPairFront) front.ServiceFront {
	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}
	return front.ServiceFront{
		ID:                   &hexed,
		Providers:            prov,
		Name:                 s.Name,
		Host:                 s.Host,
		HttpHost:             s.HttpHost,
		VODsHost:             s.VODsHost,
		CODsHost:             s.CODsHost,
		NginxHost:            s.NginxHost,
		RtmpHost:             s.RtmpHost,
		FeedbackDirecotry:    s.FeedbackDirecotry,
		TimeshiftsDirectory:  s.TimeshiftsDirectory,
		HlsDirectory:         s.HlsDirectory,
		VodsDirectory:        s.VodsDirectory,
		CodsDirectory:        s.CodsDirectory,
		ProxyDirectory:       s.ProxyDirectory,
		DataDirectory:        s.DataDirectory,
		PricePackage:         s.PricePackage,
		AutoStart:            s.AutoStart,
		ActivationKey:        s.ActivationKey,
		Monitoring:           s.Monitoring,
		CreatedDate:          &created,
		Description:          s.Description,
		ServiceStatisticInfo: *DefaultRuntimeServerStats(),
		Visible:              s.Visible,
		PID:                  s.PID,
	}
}

func MakeServerFromFront(s *front.ServiceFront) *ServiceFields {
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	return &ServiceFields{
		Name:                s.Name,
		Host:                s.Host,
		HttpHost:            s.HttpHost,
		VODsHost:            s.VODsHost,
		CODsHost:            s.CODsHost,
		NginxHost:           s.NginxHost,
		RtmpHost:            s.RtmpHost,
		FeedbackDirecotry:   s.FeedbackDirecotry,
		TimeshiftsDirectory: s.TimeshiftsDirectory,
		HlsDirectory:        s.HlsDirectory,
		VodsDirectory:       s.VodsDirectory,
		CodsDirectory:       s.CodsDirectory,
		ProxyDirectory:      s.ProxyDirectory,
		DataDirectory:       s.DataDirectory,
		PricePackage:        s.PricePackage,
		AutoStart:           s.AutoStart,
		ActivationKey:       s.ActivationKey,
		Monitoring:          s.Monitoring,
		CreatedDate:         &created,
		Description:         s.Description,
		Visible:             s.Visible,
		PID:                 s.PID,
	}
}

func (s *Service) GetHost() string {
	return fmt.Sprintf("http://%s:%s", s.Host.Host, strconv.Itoa(int(s.Host.Port)))
}

func (s *Service) GetHttpHost() string {
	return fmt.Sprintf("http://%s:%s", s.HttpHost.Host, strconv.Itoa(int(s.HttpHost.Port)))
}

func (s *Service) GetVodsHost() string {
	return fmt.Sprintf("http://%s:%s", s.VODsHost.Host, strconv.Itoa(int(s.VODsHost.Port)))
}

func (s *Service) GetCodsHost() string {
	return fmt.Sprintf("http://%s:%s", s.CODsHost.Host, strconv.Itoa(int(s.CODsHost.Port)))
}

func (s *Service) GetNginxHost() string {
	return fmt.Sprintf("http://%s:%s", s.NginxHost.Host, strconv.Itoa(int(s.NginxHost.Port)))
}

func (s *Service) GetRtmpHost() string {
	return fmt.Sprintf("rtmp://%s:%s", s.RtmpHost.Host, strconv.Itoa(int(s.RtmpHost.Port)))
}
