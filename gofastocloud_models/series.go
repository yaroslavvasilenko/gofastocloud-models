package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SerialField struct {
	Name          string               `bson:"name"           json:"name"`
	Icon          string               `bson:"icon"           json:"icon"`
	Price         float64              `bson:"price"          json:"price"`
	Groups        []string             `bson:"groups"         json:"groups"`
	Visible       bool                 `bson:"visible"        json:"visible"`
	Description   string               `bson:"description"    json:"description"`
	Season        int                  `bson:"season"         json:"season"`
	Episodes      []primitive.ObjectID `bson:"episodes"       json:"episodes"`
	ViewCount     int                  `bson:"view_count"     json:"view_count"`
	BackgroundURL string               `bson:"background_url" json:"background_url"`

	// server setup
	CreatedDate *time.Time `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

type Serial struct {
	ID          primitive.ObjectID `bson:"_id"     json:"id"`
	SerialField `bson:",inline"`
}

func NewSerialField() *SerialField {
	current := time.Now()
	return &SerialField{
		CreatedDate: &current,
		Visible:     true,
	}
}

func (s *Serial) ToFront() *front.SerialFront {
	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}

	episodes := []string{}
	for _, episode := range s.Episodes {
		episodes = append(episodes, episode.Hex())
	}
	if len(s.Groups) == 0 {
		s.Groups = []string{}
	}
	return &front.SerialFront{
		IDSerial:    &hexed,
		Name:        s.Name,
		Background:  s.BackgroundURL,
		Icon:        s.Icon,
		Groups:      s.Groups,
		Description: s.Description,
		Season:      s.Season,
		Episodes:    episodes,
		Price:       s.Price,
		ViewCount:   s.ViewCount,
		CreatedDate: &created,
	}
}

func MakeSerialFromFront(s *front.SerialFront) (*SerialField, error) {
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	var episodes []primitive.ObjectID

	for _, episode := range s.Episodes {
		e, err := primitive.ObjectIDFromHex(episode)
		if err != nil {
			return nil, err
		}
		episodes = append(episodes, e)
	}
	serial := SerialField{
		Name:          s.Name,
		Icon:          s.Icon,
		Price:         s.Price,
		Groups:        s.Groups,
		Visible:       s.Visible,
		Description:   s.Description,
		Season:        s.Season,
		Episodes:      episodes,
		ViewCount:     s.ViewCount,
		BackgroundURL: s.Background,
		CreatedDate:   &created,
	}
	return &serial, nil
}
