package front

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt"
)

type IStreamFront struct {
	Id          *string                `json:"id,omitempty"`
	Name        string                 `json:"name"`
	Price       float64                `json:"price"`
	Visible     bool                   `json:"visible"`
	IARC        int                    `json:"iarc"`
	ViewCount   int                    `json:"view_count"`
	Type        media.StreamType       `json:"type"`
	Output      []media.OutputUri      `json:"output"`
	Description string                 `json:"description"`
	TVGID       string                 `json:"tvg_id"`
	TVGName     string                 `json:"tvg_name"`
	TVGLogo     string                 `json:"tvg_logo"`
	Archive     bool                   `json:"archive"`
	Groups      []string               `json:"groups"`
	Meta        []player.MetaURL       `json:"meta"`
	Parts       []string               `json:"parts"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

type ProxyStreamFront struct {
	IStreamFront
}

func (s *ProxyStreamFront) ToPlayer() player.ChannelInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	programm := []player.ProgramInfo{}
	var urls []string
	for _, outputUrl := range s.Output {
		urls = append(urls, outputUrl.Uri)
	}
	epg := player.EPG{
		IDFiled:      s.TVGID,
		DisplayName:  s.Name,
		ICON:         s.TVGLogo,
		URLS:         urls,
		ProgramField: programm,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, Archive: s.Archive, EpgInfo: epg}
	return channel
}

func (s *ProxyStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type HardwareStreamFront struct {
	IStreamFront
	Runtime          *media.StreamStatisticInfo `json:"runtime"`
	FeedbackDir      string                     `json:"feedback_directory"`
	LogLevel         media.StreamLogLevel       `json:"log_level"`
	Input            []media.InputUri           `json:"input"`
	HaveVideo        bool                       `json:"have_video"`
	HaveAudio        bool                       `json:"have_audio"`
	AudioTracksCount int                        `json:"audio_tracks_count"`
	Loop             bool                       `json:"loop"`
	SelectedInput    int                        `json:"selected_input"`
	RestartAttempts  int                        `json:"restart_attempts"`
	AutoStart        bool                       `json:"auto_start"`

	AudioSelect  *int             `json:"audio_select,omitempty"`
	AutoExitTime *media.StreamTTL `json:"auto_exit_time,omitempty"`
}

func (s *HardwareStreamFront) ToPlayer() player.ChannelInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	programm := []player.ProgramInfo{}
	var urls []string
	for _, outputUrl := range s.Output {
		urls = append(urls, outputUrl.Uri)
	}
	epg := player.EPG{
		IDFiled:      s.TVGID,
		DisplayName:  s.Name,
		ICON:         s.TVGLogo,
		URLS:         urls,
		ProgramField: programm,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, Archive: s.Archive, EpgInfo: epg}
	return channel
}

type RelayStreamFront struct {
	HardwareStreamFront
	VideoParser media.VideoParser `json:"video_parser,omitempty"`
	AudioParser media.AudioParser `json:"audio_parser,omitempty"`
}

func (s *RelayStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type EncodeStreamFront struct {
	HardwareStreamFront
	RelayAudio bool             `json:"relay_audio"`
	RelayVideo bool             `json:"relay_video"`
	VideoCodec media.VideoCodec `json:"video_codec"`
	AudioCodec media.AudioCodec `json:"audio_codec"`

	Resample           *bool                     `json:"resample,omitempty"`
	Deinterlace        *bool                     `json:"deinterlace,omitempty"`
	Volume             *media.Volume             `json:"volume,omitempty"`
	FrameRate          *gofastogt.Rational       `json:"frame_rate,omitempty"`
	AudioChannelsCount *int                      `json:"audio_channels_count,omitempty"`
	AudioStabilization *media.AudioStabilization `json:"audio_stabilization,omitempty"`
	Size               *gofastogt.Size           `json:"size,omitempty"`
	MachineLearning    *media.MachineLearning    `json:"machine_learning,omitempty"`
	VideoBitrate       *media.Bitrate            `json:"video_bit_rate,omitempty"`
	AudioBitrate       *media.Bitrate            `json:"audio_bit_rate,omitempty"`
	Logo               *media.Logo               `json:"logo,omitempty"`
	RsvgLogo           *media.RSVGLogo           `json:"rsvg_logo,omitempty"`
	AspectRatio        *gofastogt.Rational       `json:"aspect_ratio,omitempty"`
	BackgroundEffect   *media.BackgroundEffect   `json:"background_effect,omitempty"`
	TextOverlay        *media.TextOverlay        `json:"text_overlay,omitempty"`
	VideoFlip          *media.VideoFlip          `json:"video_flip,omitempty"`
	StreamOverlay      *media.StreamOverlay      `json:"stream_overlay,omitempty"`
}

func (s *EncodeStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type TimeshiftRecorderStreamFront struct {
	RelayStreamFront
	TimeshiftChunkDuration int `json:"timeshift_chunk_duration"`
	TimeshiftChunkLifeTime int `json:"timeshift_chunk_life_time"`
}

func (s *TimeshiftRecorderStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type CatchupStreamFront struct {
	TimeshiftRecorderStreamFront
	StartRecord gofastogt.UtcTimeMsec `json:"start"`
	StopRecort  gofastogt.UtcTimeMsec `json:"stop"`
}

func (s *CatchupStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type VodBasedStreamFront struct {
	VODType       media.VodType          `json:"vod_type"`
	TrailerURL    string                 `json:"trailer_url"`
	BackgroundURL string                 `json:"background_url"`
	UserScore     float64                `json:"user_score"`
	PrimeDate     gofastogt.UtcTimeMsec  `json:"prime_date"`
	Country       string                 `json:"country"`
	Duration      gofastogt.DurationMsec `json:"duration"`
}

func (s *VodBasedStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type VodProxyStreamFront struct {
	ProxyStreamFront
	VodBasedStreamFront
}

func (s *VodProxyStreamFront) ToPlayer() player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

func (s *VodProxyStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type VodRelayStreamFront struct {
	RelayStreamFront
	VodBasedStreamFront
}

func (s *VodRelayStreamFront) ToPlayer() player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, s.HaveAudio, s.HaveVideo)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

func (s *VodRelayStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *VodEncodeStreamFront) ToPlayer() player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, s.HaveAudio, s.HaveVideo)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

type VodEncodeStreamFront struct {
	EncodeStreamFront
	VodBasedStreamFront
}

func (s *VodEncodeStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func MakeStreamBaseInfo(istream *IStreamFront, audio bool, video bool) *player.StreamBaseInfo {
	if len(istream.Parts) == 0 {
		istream.Parts = []string{}
	}
	return &player.StreamBaseInfo{
		IDStream:    *istream.Id,
		Groups:      istream.Groups,
		IARC:        istream.IARC,
		Parts:       istream.Parts,
		ViewCount:   istream.ViewCount,
		Meta:        istream.Meta,
		CreatedDate: istream.CreatedDate,
		Audio:       audio,
		Video:       video,

		//
		Favorite:         false,
		Locked:           false,
		Recent:           0,
		InterruptionTime: 0,
	}
}

func MakeMovieInfo(baseVOD *VodBasedStreamFront, istream *IStreamFront) *player.MovieInfo {
	var urls []string
	for _, outputUrl := range istream.Output {
		urls = append(urls, outputUrl.Uri)
	}
	return &player.MovieInfo{
		DisplayName:   istream.Name,
		Description:   istream.Description,
		PreviewIcon:   istream.TVGLogo,
		URLS:          urls,
		BackgroundURL: baseVOD.BackgroundURL,
		TrailerUrl:    baseVOD.TrailerURL,
		UserScore:     baseVOD.UserScore,
		PrimeDate:     baseVOD.PrimeDate,
		Country:       baseVOD.Country,
		Duration:      baseVOD.Duration,
		VODType:       baseVOD.VODType,
	}
}
