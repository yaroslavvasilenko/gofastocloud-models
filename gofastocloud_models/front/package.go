package front

import (
	"net"
	"net/url"
	"strconv"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt"
)

type WSServerFront struct {
	Url      string  `bson:"url"                json:"url"`
	Login    *string `bson:"login,omitempty"    json:"login,omitempty"`
	Password *string `bson:"password,omitempty" json:"password,omitempty"`
}

func (ws *WSServerFront) GetHostAndPort() (*gofastogt.HostAndPort, error) {
	u, err := url.Parse(ws.Url)
	if err != nil {
		return nil, err
	}

	host, portStr, err := net.SplitHostPort(u.Host)
	if err != nil {
		return nil, err
	}

	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, err
	}

	hs := gofastogt.HostAndPort{Host: host, Port: uint16(port)}
	return &hs, nil
}

func (ws *WSServerFront) GetStreamsUrl() string {
	copy := ws.Url

	if copy[len(copy)-1] != '/' {
		copy += "/"
	}

	return copy + "server/db/stream/list"
}

func (ws *WSServerFront) GetViewStreamsUrl(sid string) string {
	copy := ws.Url

	if copy[len(copy)-1] != '/' {
		copy += "/"
	}
	return copy + "server/db/stream/view/" + sid
}

func NewWSServer(url string, login string, password string) *WSServerFront {
	return &WSServerFront{Url: url, Login: &login, Password: &password}
}

func NewFastoCloudWSServer() *WSServerFront {
	return NewWSServer("https://api.fastocloud.com", "fastocloud", "fastocloud")
}

type PackageFront struct {
	ID            *string                `json:"id,omitempty"`
	Name          string                 `json:"name"`
	Local         bool                   `json:"local"`
	Host          WSServerFront          `json:"host"`
	Price         *player.PricePack      `json:"price,omitempty"`
	Description   string                 `json:"description"`
	BackgroundURL string                 `json:"background_url"`
	Visible       bool                   `json:"visible"`
	CreatedDate   *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

func (pack *PackageFront) IsPaid() bool {
	return pack.Price != nil
}

type PaymentType int

const (
	STRIPE PaymentType = iota
	PAYPAL
)

type PaymentFront struct {
	Type PaymentType `json:"type" yaml:"type"`

	Stripe *PaymentStripeData `json:"stripe,omitempty" yaml:"stripe,omitempty"`
	Paypal *PaymentPaypalData `json:"paypal,omitempty" yaml:"paypal,omitempty"`
}

type PaymentStripeData struct {
	PubKey         string `json:"pub_key"     yaml:"pub_key"`
	SecretKey      string `json:"secret_key"  yaml:"secret_key"`
	EndpointSecret string `yaml:"endpoint_secret"`
	WebhookId      string `yaml:"webhook_id"`
}

type PaypalMode int

const (
	SANDBOX PaypalMode = iota
	LIVE
)

type PaymentPaypalData struct {
	ClientId     string     `json:"client_id"     yaml:"client_id"`
	ClientSecret string     `json:"client_secret" yaml:"client_secret"`
	Mode         PaypalMode `json:"mode"          yaml:"mode"`
	WebhookId    string     `yaml:"webhook_id"`
}
