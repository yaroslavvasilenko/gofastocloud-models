package front

import (
	"encoding/json"
	"errors"
	"net/mail"

	"gitlab.com/fastogt/gofastogt"
)

type StatusSubscriber int

const (
	NOT_ACTIVE_SUB StatusSubscriber = iota
	ACTIVE_SUB
	DELETED_SUB
)

type DeviceStatus int

const (
	NOT_ACTIVE_DEVICE DeviceStatus = iota
	ACTIVE_DEVICE
	BANNED_DEVICE
)

type DevicesFront struct {
	IDDevice    *string                `json:"id,omitempty"`
	Name        string                 `json:"name"`
	Status      DeviceStatus           `json:"status"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

type UserStreamFront struct {
	IDStream         string                 `json:"id"`
	Favorite         bool                   `json:"favorite"`
	Private          bool                   `json:"private"`
	Locked           bool                   `json:"locked"`
	Recent           gofastogt.UtcTimeMsec  `json:"recent"`
	InterruptionTime gofastogt.DurationMsec `json:"interruption_time"`
}

type SubscriberFront struct {
	Id             *string                `json:"id,omitempty"`
	Email          string                 `json:"email"`
	FirstName      string                 `json:"first_name"`
	LastName       string                 `json:"last_name"`
	Password       *string                `json:"password,omitempty"`
	CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	ExpDate        gofastogt.UtcTimeMsec  `json:"exp_date"`
	Status         StatusSubscriber       `json:"status"`
	DeviceCount    int                    `json:"devices_count"`
	MaxDeviceCount int                    `json:"max_devices_count"`
	Country        string                 `json:"country"`
	Language       string                 `json:"language"`
	Servers        []string               `json:"servers"`
}

func (s *SubscriberFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Id             *string                `json:"id,omitempty"`
		Email          *string                `json:"email"`
		FirstName      *string                `json:"first_name"`
		LastName       *string                `json:"last_name"`
		Password       *string                `json:"password,omitempty"`
		CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
		ExpDate        *gofastogt.UtcTimeMsec `json:"exp_date"`
		Status         *StatusSubscriber      `json:"status"`
		DeviceCount    *int                   `json:"devices_count"`
		MaxDeviceCount *int                   `json:"max_devices_count"`
		Country        *string                `json:"country"`
		Language       *string                `json:"language"`
		Servers        *[]string              `json:"servers"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password != nil {
		if len(*request.Password) == 0 {
			return errors.New("password can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.ExpDate == nil {
		return errors.New("exp_date can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.DeviceCount == nil {
		return errors.New("deviceCount can't be empty")
	}
	if request.MaxDeviceCount == nil {
		return errors.New("maxDeviceCount can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.Servers == nil {
		return errors.New("servers can't be empty")
	}

	s.Id = request.Id
	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = request.Password
	s.CreatedDate = request.CreatedDate
	s.ExpDate = *request.ExpDate
	s.Status = *request.Status
	s.DeviceCount = *request.DeviceCount
	s.MaxDeviceCount = *request.MaxDeviceCount
	s.Country = *request.Country
	s.Language = *request.Language
	s.Servers = *request.Servers

	return nil
}
