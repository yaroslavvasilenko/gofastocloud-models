package front

import "gitlab.com/fastogt/gofastogt"

type SerialFront struct {
	IDSerial    *string                `json:"id,omitempty"`
	Name        string                 `json:"name"`
	Background  string                 `json:"background"`
	Icon        string                 `json:"icon"`
	Groups      []string               `json:"groups"`
	Description string                 `json:"description"`
	Season      int                    `json:"season"`
	Episodes    []string               `json:"episodes"`
	Price       float64                `json:"price"`
	ViewCount   int                    `json:"view_count"`
	Visible     bool                   `json:"visible"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}
