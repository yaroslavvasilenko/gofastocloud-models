package front

import (
	"encoding/json"
	"errors"
	"net/mail"

	"gitlab.com/fastogt/gofastogt"
)

type ProviderType int

type ProviderStatus int

type ProviderRole int

const (
	READ ProviderRole = iota
	WRITE
	SUPPORT
	ADMINS
)

const (
	NO_ACTIVE ProviderStatus = iota
	ACTIVE
	BANNED
)

const (
	ADMIN ProviderType = iota
	RESELLER
)

type ProviderFront struct {
	ID               *string                `json:"id,omitempty"`
	Email            string                 `json:"email"`
	FirstName        string                 `json:"first_name"`
	LastName         string                 `json:"last_name"`
	Password         *string                `json:"password,omitempty"`
	CreatedDate      *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Type             ProviderType           `json:"type"`
	Status           ProviderStatus         `json:"status"`
	Country          string                 `json:"country"`
	Language         string                 `json:"language"`
	Credits          int                    `json:"credits"`
	CreditsRemaining int                    `json:"credits_remaining"`
}

type ProviderPairFront struct {
	Email        string `json:"email"`
	Id           string `json:"id"`
	ProviderRole `json:"role"`
}

//CreaditsRemanin don't post from font.
func (p *ProviderFront) UnmarshalJSON(data []byte) error {
	request := struct {
		ID          *string                `json:"id,omitempty"`
		Email       *string                `json:"email"`
		FirstName   *string                `json:"first_name"`
		LastName    *string                `json:"last_name"`
		Password    *string                `json:"password,omitempty"`
		CreatedDate *gofastogt.UtcTimeMsec `json:"created_date"`
		Type        *ProviderType          `json:"type"`
		Status      *ProviderStatus        `json:"status"`
		Country     *string                `json:"country"`
		Language    *string                `json:"language"`
		Credits     *int                   `json:"credits"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password != nil {
		if len(*request.Password) == 0 {
			return errors.New("password can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Type == nil {
		return errors.New("type can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.Credits == nil {
		return errors.New("credits can't be empty")
	}

	p.ID = request.ID
	p.Email = *request.Email
	p.FirstName = *request.FirstName
	p.LastName = *request.LastName
	p.Password = request.Password
	p.CreatedDate = request.CreatedDate
	p.Type = *request.Type
	p.Status = *request.Status
	p.Country = *request.Country
	p.Language = *request.Language
	p.Credits = *request.Credits

	return nil
}
