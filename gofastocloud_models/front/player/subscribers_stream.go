package player

import (
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastogt"
)

//Life Stream models for subscriber devices

type MetaURL struct {
	Name string `bson:"name" json:"name"`
	URL  string `bson:"url" json:"url"`
}

type StreamBaseInfo struct {
	IDStream    string                 `json:"id"`
	Groups      []string               `json:"groups"`
	IARC        int                    `json:"iarc"`
	Parts       []string               `json:"parts"`
	ViewCount   int                    `json:"view_count"`
	Meta        []MetaURL              `json:"meta"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Video       bool                   `json:"video"`
	Audio       bool                   `json:"audio"`

	// user
	Favorite         bool                   `json:"favorite"`
	Locked           bool                   `json:"locked"`
	Recent           gofastogt.UtcTimeMsec  `json:"recent"`
	InterruptionTime gofastogt.DurationMsec `json:"interrupt_time"`
}

type EPG struct {
	IDFiled      string        `json:"id"`
	URLS         []string      `json:"urls"`
	DisplayName  string        `json:"display_name"`
	ICON         string        `json:"icon"`
	ProgramField []ProgramInfo `json:"programs"`
}

type ProgramInfo struct {
	ChannelField string                `json:"channel"`
	Start        gofastogt.UtcTimeMsec `json:"start"`
	Stop         gofastogt.UtcTimeMsec `json:"stop"`
	Title        string                `json:"title"`
	Category     string                `json:"category"`
	Description  string                `json:"description"`
}

type ChannelInfo struct {
	StreamBaseInfo
	EpgInfo EPG  `json:"epg"`
	Archive bool `json:"archive"`
}

//VODS Stream models for subscriber devices
type MovieInfo struct {
	URLS          []string               `json:"urls"`
	Description   string                 `json:"description"`
	DisplayName   string                 `json:"display_name"`
	BackgroundURL string                 `json:"backgorund_url"`
	PreviewIcon   string                 `json:"preview_icon"`
	TrailerUrl    string                 `json:"trailer_url"`
	UserScore     float64                `json:"user_score"`
	PrimeDate     gofastogt.UtcTimeMsec  `json:"prime_date"`
	Country       string                 `json:"country"`
	Duration      gofastogt.DurationMsec `json:"duration"`
	VODType       media.VodType          `json:"type"`
}

type VodInfo struct {
	StreamBaseInfo
	VOD MovieInfo `json:"vod"`
}

//Catchup Stream models for subscriber devices
type CatchupInfo struct {
	ChannelInfo
	StartRecord gofastogt.UtcTimeMsec `json:"start"`
	StopRecort  gofastogt.UtcTimeMsec `json:"stop"`
}
