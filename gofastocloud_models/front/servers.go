package front

import (
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt"
)

type ServiceFront struct {
	ID                  *string                `json:"id,omitempty"`
	Providers           []ProviderPairFront    `json:"providers"`
	Name                string                 `json:"name"`
	Host                gofastogt.HostAndPort  `json:"host"`
	HttpHost            gofastogt.HostAndPort  `json:"http_host"`
	VODsHost            gofastogt.HostAndPort  `json:"vods_host"`
	CODsHost            gofastogt.HostAndPort  `json:"cods_host"`
	NginxHost           gofastogt.HostAndPort  `json:"nginx_host"`
	RtmpHost            gofastogt.HostAndPort  `json:"rtmp_host"`
	FeedbackDirecotry   string                 `json:"feedback_directory"`
	TimeshiftsDirectory string                 `json:"timeshifts_directory"`
	HlsDirectory        string                 `json:"hls_directory"`
	VodsDirectory       string                 `json:"vods_directory"`
	CodsDirectory       string                 `json:"cods_directory"`
	ProxyDirectory      string                 `json:"proxy_directory"`
	DataDirectory       string                 `json:"data_directory"`
	PricePackage        float64                `json:"price_package"`
	AutoStart           bool                   `json:"auto_start"`
	ActivationKey       string                 `json:"activation_key"`
	Monitoring          bool                   `json:"monitoring"`
	CreatedDate         *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Description         string                 `json:"description"`
	Visible             bool                   `json:"visible"`
	PID                 string                 `json:"pid"`
	media.ServiceStatisticInfo
	Status     gofastocloud_base.ConnectionStatus `json:"status"`
	Version    string                             `json:"version"`
	Project    string                             `json:"project"`
	ExpireTime gofastogt.UtcTimeMsec              `json:"expiration_time"`
	SyncTime   gofastogt.UtcTimeMsec              `json:"synctime"`
	OS         gofastocloud_base.OperationSystem  `json:"os"`
}
