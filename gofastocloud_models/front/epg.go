package front

import (
	"gitlab.com/fastogt/gofastocloud/epg"
	"gitlab.com/fastogt/gofastogt"
)

type EpgUrlFront struct {
	ID  string `json:"id"`
	Url string `json:"url"`
}

type EpgFront struct {
	ID               *string                `json:"id,omitempty"`
	Name             string                 `json:"name"`
	Host             gofastogt.HostAndPort  `json:"host"`
	URLS             []EpgUrlFront          `json:"urls"`
	Providers        []ProviderPairFront    `json:"providers"`
	CreatedDate      *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Monitoring       bool                   `json:"monitoring"`
	AutoStart        bool                   `json:"auto_start"`
	ActivationKey    string                 `json:"activation_key"`
	AutoUpdate       bool                   `json:"auto_update"`
	AutoUpdatePeriod gofastogt.UtcTimeMsec  `json:"auto_update_period"`
	epg.ServiceStatisticInfo
}
