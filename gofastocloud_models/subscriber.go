package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/constans"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/ott"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserStream struct {
	IDStream         primitive.ObjectID     `bson:"sid"               json:"id"`
	Favorite         bool                   `bson:"favorite"          json:"favorite"`
	Private          bool                   `bson:"private"           json:"private"`
	Locked           bool                   `bson:"locked"            json:"locked"`
	Recent           time.Time              `bson:"recent"            json:"recent"`
	InterruptionTime gofastogt.DurationMsec `bson:"interruption_time" json:"interruption_time"`
}

func NewUserStream() *UserStream {
	return &UserStream{
		Favorite:         false,
		Private:          false,
		Locked:           false,
		InterruptionTime: constans.MAX_VIDEO_DURATION_MSEC,
		Recent:           time.Now(),
	}
}

type SubscriberFields struct {
	Email          string                 `bson:"email"             json:"email"`
	FirstName      string                 `bson:"first_name"        json:"first_name"`
	LastName       string                 `bson:"last_name"         json:"last_name"`
	Password       string                 `bson:"password"          json:"password"`
	ExpDate        time.Time              `bson:"exp_date"          json:"exp_date"`
	Status         front.StatusSubscriber `bson:"status"            json:"status"`
	MaxDeviceCount int                    `bson:"max_devices_count" json:"max_devices_count"`
	Country        string                 `bson:"country"           json:"country"`
	Language       string                 `bson:"language"          json:"language"`
	Servers        []primitive.ObjectID   `bson:"servers"           json:"servers"`
	Devices        []ott.Device           `bson:"devices"           json:"devices"`
	CID            string                 `bson:"cid"               json:"cid"`
	Streams        []UserStream           `bson:"streams"           json:"streams"`
	Vods           []UserStream           `bson:"vods"              json:"vods"`
	Catchups       []UserStream           `bson:"catchups"          json:"catchups"`
	Series         []primitive.ObjectID   `bson:"series"            json:"series"`
	Subscriptions  []primitive.ObjectID   `bson:"subscriptions"     json:"subscriptions"`

	// server setup
	CreatedDate *time.Time `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

type Subscriber struct {
	ID               primitive.ObjectID `bson:"_id"     json:"id"`
	SubscriberFields `bson:",inline"`
}

func NewSubscriver() *SubscriberFields {
	current := time.Now()
	return &SubscriberFields{
		CreatedDate: &current,
		ExpDate:     time.Date(2100, time.January, 1, 0, 0, 0, 0, time.Local),
		Status:      front.NOT_ACTIVE_SUB,
		Language:    "en",
	}
}

func RemoveSubStreamFromArray(s []UserStream, r primitive.ObjectID) []UserStream {
	for i, v := range s {
		if v.IDStream == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func (sub *Subscriber) ToFront() *front.SubscriberFront {
	servers := []string{}
	for _, s := range sub.Servers {
		servers = append(servers, primitive.ObjectID.Hex(s))
	}
	var created gofastogt.UtcTimeMsec
	if sub.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*sub.CreatedDate)
	}
	hexed := sub.ID.Hex()
	return &front.SubscriberFront{
		Id:             &hexed,
		Email:          sub.Email,
		FirstName:      sub.FirstName,
		LastName:       sub.LastName,
		Password:       &sub.Password,
		CreatedDate:    &created,
		ExpDate:        gofastogt.Time2UtcTimeMsec(sub.ExpDate),
		Status:         sub.Status,
		MaxDeviceCount: sub.MaxDeviceCount,
		DeviceCount:    len(sub.Devices),
		Country:        sub.Country,
		Language:       sub.Language,
		Servers:        servers,
	}
}

func (stream *UserStream) ToFront() *front.UserStreamFront {
	return &front.UserStreamFront{
		IDStream:         stream.IDStream.Hex(),
		Favorite:         stream.Favorite,
		Private:          stream.Private,
		Locked:           stream.Locked,
		Recent:           gofastogt.Time2UtcTimeMsec(stream.Recent),
		InterruptionTime: stream.InterruptionTime,
	}
}

//

func MakeSubscriberFromFront(s *front.SubscriberFront) (*SubscriberFields, error) {
	subServers := []primitive.ObjectID{}
	for _, s := range s.Servers {
		serverID, err := primitive.ObjectIDFromHex(s)
		if err != nil {
			return nil, err
		}
		subServers = append(subServers, serverID)
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	subscriber := SubscriberFields{
		Email:          s.Email,
		FirstName:      s.FirstName,
		LastName:       s.LastName,
		CreatedDate:    &created,
		ExpDate:        gofastogt.UtcTime2Time(s.ExpDate),
		MaxDeviceCount: s.MaxDeviceCount,
		Servers:        subServers,
		Country:        s.Country,
		Language:       s.Language,
		Status:         s.Status,
		Password:       *s.Password,
	}
	return &subscriber, nil
}

//FEXME Programm array
func MakeChannelInfo(istream *IStream, ustream *UserStream, video, audio *bool) *player.ChannelInfo {
	programm := []player.ProgramInfo{}
	var urls []string
	for _, outputUrl := range istream.Output {
		urls = append(urls, outputUrl.Uri)
	}
	epg := player.EPG{
		IDFiled:      istream.TVGID,
		DisplayName:  istream.Name,
		ICON:         istream.TVGLogo,
		URLS:         urls,
		ProgramField: programm,
	}
	return &player.ChannelInfo{
		StreamBaseInfo: *MakeStreamBaseInfo(istream, ustream, video, audio),
		Archive:        istream.Archive,
		EpgInfo:        epg,
	}
}

func MakeVodInfo(istream *IStream, vodBaseStream *VodBasedStream, ustream *UserStream, video, audio *bool) *player.VodInfo {
	return &player.VodInfo{
		StreamBaseInfo: *MakeStreamBaseInfo(istream, ustream, video, audio),
		VOD:            *MakeMovieInfo(vodBaseStream, istream),
	}
}

func MakeCatchupInfo(catchup *CatchupStream, ustream *UserStream, video, audio *bool) *player.CatchupInfo {
	return &player.CatchupInfo{
		ChannelInfo: *MakeChannelInfo(&catchup.IStream, ustream, video, audio),
		StartRecord: gofastogt.Time2UtcTimeMsec(catchup.StartRecord),
		StopRecort:  gofastogt.Time2UtcTimeMsec(catchup.StopRecort),
	}
}

func MakeMovieInfo(baseVOD *VodBasedStream, istream *IStream) *player.MovieInfo {
	var urls []string
	for _, outputUrl := range istream.Output {
		urls = append(urls, outputUrl.Uri)
	}
	return &player.MovieInfo{
		DisplayName:   istream.Name,
		Description:   istream.Description,
		PreviewIcon:   istream.TVGLogo,
		URLS:          urls,
		BackgroundURL: baseVOD.BackgroundURL,
		TrailerUrl:    baseVOD.TrailerURL,
		UserScore:     baseVOD.UserScore,
		PrimeDate:     gofastogt.Time2UtcTimeMsec(baseVOD.PrimeDate),
		Country:       baseVOD.Country,
		Duration:      baseVOD.Duration,
		VODType:       baseVOD.VODType,
	}
}

func MakeStreamBaseInfo(istream *IStream, ustream *UserStream, video, audio *bool) *player.StreamBaseInfo {
	var created gofastogt.UtcTimeMsec
	if istream.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*istream.CreatedDate)
	}

	if len(istream.Parts) == 0 {
		istream.Parts = []string{}
	}
	if len(istream.Meta) == 0 {
		istream.Meta = []player.MetaURL{}
	}
	if len(istream.Groups) == 0 {
		istream.Groups = []string{}
	}
	return &player.StreamBaseInfo{
		IDStream:         istream.ID.Hex(),
		Groups:           istream.Groups,
		IARC:             istream.IARC,
		Favorite:         ustream.Favorite,
		Locked:           ustream.Locked,
		Recent:           gofastogt.Time2UtcTimeMsec(ustream.Recent),
		InterruptionTime: ustream.InterruptionTime,
		Video:            *video,
		Audio:            *audio,
		Parts:            istream.Parts,
		ViewCount:        istream.ViewCount,
		Meta:             istream.Meta,
		CreatedDate:      &created,
	}
}
