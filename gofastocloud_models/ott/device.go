package ott

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Device struct {
	IDDevice primitive.ObjectID `bson:"_id"          json:"id"`
	Name     string             `bson:"name"         json:"name"`
	Status   front.DeviceStatus `bson:"status"       json:"status"`

	CreatedDate *time.Time `bson:"created_date,omitempty" json:"created_date,omitempty"`
}

func NewDevice() *Device {
	current := time.Now()
	return &Device{
		IDDevice:    primitive.NewObjectID(),
		Name:        "Device",
		CreatedDate: &current,
		Status:      front.NOT_ACTIVE_DEVICE,
	}
}

func (device *Device) ToFront() *front.DevicesFront {
	hexed := device.IDDevice.Hex()
	var created gofastogt.UtcTimeMsec
	if device.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*device.CreatedDate)
	}

	return &front.DevicesFront{
		IDDevice:    &hexed,
		Name:        device.Name,
		Status:      device.Status,
		CreatedDate: &created,
	}
}

func MakeDeviceFromFront(s *front.DevicesFront) *Device {
	var did primitive.ObjectID
	if s.IDDevice != nil {
		did, _ = primitive.ObjectIDFromHex(*s.IDDevice)
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}

	return &Device{
		Name:        s.Name,
		CreatedDate: &created,
		IDDevice:    did,
		Status:      s.Status,
	}
}
