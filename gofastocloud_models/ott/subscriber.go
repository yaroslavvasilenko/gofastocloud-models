package ott

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SubscriberFields struct {
	Email          string                 `bson:"email"             json:"email"`
	FirstName      string                 `bson:"first_name"        json:"first_name"`
	LastName       string                 `bson:"last_name"         json:"last_name"`
	Password       string                 `bson:"password"          json:"password"`
	ExpDate        time.Time              `bson:"exp_date"          json:"exp_date"`
	Status         front.StatusSubscriber `bson:"status"            json:"status"`
	Country        string                 `bson:"country"           json:"country"`
	Language       string                 `bson:"language"          json:"language"`
	Servers        []primitive.ObjectID   `bson:"servers"           json:"servers"`
	MaxDeviceCount int                    `bson:"max_devices_count" json:"max_devices_count"`

	// filled by server
	CID           string               `bson:"cid"               json:"cid"`
	Subscriptions []primitive.ObjectID `bson:"subscriptions"     json:"subscriptions"`
	Devices       []Device             `bson:"devices"           json:"devices"`
	CreatedDate   *time.Time           `bson:"created_date,omitempty" json:"created_date,omitempty"`
}

type Subscriber struct {
	ID               primitive.ObjectID `bson:"_id"     json:"id"`
	SubscriberFields `bson:",inline"`
}

func (sub *Subscriber) ToFront() *front.SubscriberFront {
	servers := []string{}
	for _, s := range sub.Servers {
		servers = append(servers, primitive.ObjectID.Hex(s))
	}
	var created gofastogt.UtcTimeMsec
	if sub.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*sub.CreatedDate)
	}
	hexed := sub.ID.Hex()
	return &front.SubscriberFront{
		Id:             &hexed,
		Email:          sub.Email,
		FirstName:      sub.FirstName,
		LastName:       sub.LastName,
		Password:       &sub.Password,
		CreatedDate:    &created,
		ExpDate:        gofastogt.Time2UtcTimeMsec(sub.ExpDate),
		Status:         sub.Status,
		MaxDeviceCount: sub.MaxDeviceCount,
		DeviceCount:    len(sub.Devices),
		Country:        sub.Country,
		Language:       sub.Language,
		Servers:        servers,
	}
}

//

func MakeSubscriberFromFront(s *front.SubscriberFront) (*SubscriberFields, error) {
	subServers := []primitive.ObjectID{}
	for _, s := range s.Servers {
		serverID, err := primitive.ObjectIDFromHex(s)
		if err != nil {
			return nil, err
		}
		subServers = append(subServers, serverID)
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	subscriber := SubscriberFields{
		Email:          s.Email,
		FirstName:      s.FirstName,
		LastName:       s.LastName,
		CreatedDate:    &created,
		ExpDate:        gofastogt.UtcTime2Time(s.ExpDate),
		MaxDeviceCount: s.MaxDeviceCount,
		Servers:        subServers,
		Country:        s.Country,
		Language:       s.Language,
		Status:         s.Status,
		Password:       *s.Password,
	}
	return &subscriber, nil
}

func (s *SubscriberFields) ContainsSubscription(sid primitive.ObjectID) bool {
	for _, sub := range s.Subscriptions {
		if sub == sid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) ContainsPackage(pid primitive.ObjectID) bool {
	for _, pack := range s.Servers {
		if pack == pid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) GetDevice(id primitive.ObjectID) *Device {
	for _, device := range s.Devices {
		if device.IDDevice == id {
			return &device
		}
	}
	return nil
}
