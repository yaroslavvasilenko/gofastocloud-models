module gitlab.com/fastogt/gofastocloud_models

go 1.16

require (
	github.com/stretchr/testify v1.7.5
	gitlab.com/fastogt/gofastocloud v1.8.3
	gitlab.com/fastogt/gofastocloud_base v1.6.0
	gitlab.com/fastogt/gofastogt v1.3.0
	go.mongodb.org/mongo-driver v1.9.1
)
